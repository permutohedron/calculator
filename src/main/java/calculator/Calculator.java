package calculator;

import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

public class Calculator {
    private static final String DEFAULT_SEPARATOR_REGEX = "[,|\n]+";
    private static final String SEPARATORS_DEFINITION_PREFIX = "//[";
    private static final String SEPARATORS_DEFINITION_SUFFIX = "]\n";
    private static final String SEPARATORS_DEFINITION_SEPARATOR = Pattern.quote("][");

    public static int sum(String arg) {
        if (arg.isEmpty()) return 0;

        String separatorRegex = extractSeparatorRegEx(arg);
        String numbersWithSeparators = extractNumbersWithSeparators(arg);

        return Arrays.stream(numbersWithSeparators.split(separatorRegex))
                .mapToInt(Integer::parseInt).sum();
    }

    private static String extractNumbersWithSeparators(String arg) {
        if (arg.startsWith(SEPARATORS_DEFINITION_PREFIX)) {
            return arg.substring(
                    arg.indexOf(SEPARATORS_DEFINITION_SUFFIX)
                            + SEPARATORS_DEFINITION_SUFFIX.length()
            );
        } else {
            return arg;
        }
    }

    private static String extractSeparatorRegEx(String arg) {
        if (arg.startsWith(SEPARATORS_DEFINITION_PREFIX)) {
            return getDisjoinedSeparators(arg).stream()
                    .map(escapeSpecialCharacters())
                    .sorted(fromLongestToShortest())
                    .collect(Collectors.joining("|", "(", ")+"));

        } else {
            return DEFAULT_SEPARATOR_REGEX;
        }
    }

    private static List<String> getDisjoinedSeparators(String arg) {
        String disjoinedSeparatorsString =
                arg.substring(SEPARATORS_DEFINITION_PREFIX.length(), arg.indexOf(SEPARATORS_DEFINITION_SUFFIX));

        List<String> separators = new ArrayList<>();
        Collections.addAll(separators, disjoinedSeparatorsString.split(SEPARATORS_DEFINITION_SEPARATOR));
        separators.add("\n");

        return separators;
    }

    private static Comparator<String> fromLongestToShortest() {
        return (s1, s2) -> s2.length() - s1.length();
    }

    private static Function<String, String> escapeSpecialCharacters() {
        return Pattern::quote;
    }

}
