package calculator

import spock.lang.Specification

class CalculatorTest extends Specification {
    private Calculator calculator

    void setup() {
        calculator = new Calculator()
    }

    def "summation of empty string results in zero"() {
        expect:
        calculator.sum("") == 0
    }

    def "summation of one number returns that number"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                          || expected
        "0"                          || 0
        "1"                          || 1
        "-1"                         || -1
        "7"                          || 7
        Integer.MAX_VALUE.toString() || Integer.MAX_VALUE
        Integer.MIN_VALUE.toString() || Integer.MIN_VALUE
    }

    def "summation of comma separated numbers"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                || expected
        "-1,7"             || 6
        "1,-7"             || -6
        "-2,-2"            || -4
        "1,2,3,-1,-7,8,12" || 18
        "-5,-6,-5,1000"    || 1000 - 16
    }

    def "summation of comma and \"\\n\" separated numbers"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                || expected
        "-2\n7"            || 5
        "2\n-7"            || -5
        "-2,-2\n-2"        || -6
        "-3,-5\n567\n-300" || -3 - 5 + 567 - 300
        "7\n2\n9\n4"       || 7 + 2 + 9 + 4
    }

    def "summation of custom separated numbers (one separator)"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                                     || expected
        "//[!]\n2!3"         || 5
        "//[!]\n3!4!-9\n6"                      || 3 + 4 - 9 + 6
        "//[!56]\n-3!567!56-200"                || -3 + 7 - 200
        "//[\n\t\n]\n-3\n\t\n-654\n24\n\t\n567" || -3 - 654 + 24 + 567
        "//[[]]\n1[]2\n3[]4" || 1 + 2 + 3 + 4
    }

    def "summation with regex special characters in separator"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                     || expected
        "//[.|*]\n56.|*-65.|*1" || 56 - 65 + 1
        "//[*]\n1*2*3"          || 1 + 2 + 3
        "//[.+]\n1.+2\n3"       || 1 + 2 + 3
        "//[^}]\n1^}2\n3^}4"    || 1 + 2 + 3 + 4
    }

    def "summation of custom separated numbers (many separators)"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                       || expected
        "//[a][bv]\n5a-67bv4\n-7" || 5 - 67 + 4 - 7
        "//[a][[]]\n5a-67[]4\n-7" || 5 - 67 + 4 - 7
        "//[.][.][.][.]\n0.1.2"   || 0 + 1 + 2
        "//[@][&]\n5@6&\n4"       || 5 + 6 + 4
    }

    def "summation with separators that includes other separator"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                                                || expected
        "//[a][ab][abc][dabf]\n5a-67ab4\n-7abc15dabf1"     || 5 - 67 + 4 - 7 + 15 + 1
        "//[.][.*][.*.][.[.*].]\n5.-67.*4\n-7.*.15.[.*].1" || 5 - 67 + 4 - 7 + 15 + 1
        "//[.][..][.][.]\n0.1.2..3"                        || 0 + 1 + 2 + 3
    }

    def "summation of wrong string throws exception"(String arg) {

        when:
        calculator.sum(arg);

        then:
        thrown Exception

        where:
        arg << ["/", "//[!v]\n1!2!3", "1.2.4", "//[.][.,]\n1,2.,3"]

    }

    def "summation with many separators in a row"(String arg, int expected) {
        expect:
        calculator.sum(arg) == expected

        where:
        arg                         || expected
        "1,2,,3,,,4,,,,5"           || 1 + 2 + 3 + 4 + 5
        "1\n2\n\n3\n\n\n4\n\n\n\n5" || 1 + 2 + 3 + 4 + 5
        "1\n2,,3\n\n\n4\n,\n,5"     || 1 + 2 + 3 + 4 + 5
        "//[7]\n1\n27737777477\n75" || 1 + 2 + 3 + 4 + 5

    }

}
